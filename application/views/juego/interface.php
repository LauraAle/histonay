

<link href="<?php echo base_url() ?>assets/css/styleIndex.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet">
<a href="https://www.flaticon.com/authors/freepik" title="Freepik"></a>
<a href="https://www.flaticon.com/" title="Flaticon"></a>
<body style="background-image: url(<?php echo base_url() ?>assets/img/aguila.jpg)">
    <div class="container" id="div_numJugadores">
        <br>
        <br>
        <br>

        <div class="row">
           <div class="col-md-12 contact-form">
            <img src="<?php echo base_url() ?>assets/img/bandera_mexico.png" class="avatar">
            <h2>INGRESAR</h2>
            <form class="formulario">
                <h5>Número de Jugadores</h5>
                <select class="form-control" id="numeroJugadores" required>
                  <option value="2" selected="true">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                </select>
                <input class="btn btn-warning btn-block" id="num_jugadores" type="button" value="Enviar" style="margin-top: 30px; background-color: red; border-color:red; color: white">
            </form>
            </div>
        </div>
    </div>

    <div class="container" id="div_nombreJugadores">
        <br>
        <br>
        <br>
        <div class="row">
           <div class="col-md-12 contact-form-jugadores">
              <img src="<?php echo base_url() ?>assets/img/sombrero.png" class="avatar">
              <h2>JUGADORES</h2>
              <form class="formulario" required id="espacio_jugadores">
                
              </form>
              <input class="btn btn-warning btn-block" id="enviar_nombres" type="button" value="Enviar" style="margin-top: 30px; background-color: red; border-color:red; color: white">
            </div>
        </div>
    </div>

    <div class="container" id="div_preguntas" pregunta_id="" jugador_id="" aciertos="" jugador="">
        <br>
        <br>
        <br>

        <div class="row">
           <div class="col-md-12 contact-form-preguntas">
            <img src="<?php echo base_url() ?>assets/img/nopal.png" class="avatar">
            <h4 style="color: white; margin-top: -70px; margin-left: 600px" id="aciertos"></h4>
            <form class="formulario" required>
                <div class="col-md-12">
                    <h2 id="jugador_turno"></h2>
                    <table class="table" style="border-color: black">
                      <tr>
                        <td style="background-color: green; color:white" id="pregunta"></td>
                      </tr>
                      <tr>
                      <tr><td><input type="radio" id="label1" name="verificar" value="1" required><label for="label1" id="respuesta1"></label></td></tr>
                      <tr><td><input id="label2" type="radio" name="verificar" value="2" required><label for="label2" id="respuesta2"></label></td></tr>
                      <tr><td><input id="label3" type="radio" name="verificar" value="3" required><label for="label3" id="respuesta3"></label></td></tr>
                      <tr><td><input id="label4" type="radio" name="verificar" value="4" required><label for="label4" id="respuesta4"></label></td></tr>
                      </tr>
                    </table>
                    <div class="row">
                        <div class="col-md-5"></div>
                        <div class="col-md-2"><input style="color: white; background-color: red; border-color: red" class="btn btn-warning btn-block" type="button" value="Enviar" id="enviar_respuesta"></div>
                        <div class="col-md-5"></div>                      
                    </div>
                </div>                              
            </form>
            </div>
        </div>
    </div>

</body>