	
	
<script>
	$(document).ready(function() {
		var url = '<?php echo base_url() ?>juego/';
		var numero_jugadores = 0;
		var jugadores_array = [];
		app = {
			iniciar_juego : function(){
				$('#div_nombreJugadores *').hide();
				$('#div_preguntas *').hide();
			},
			enviar_jugadores(){
				$.ajax({
					url  : url+'enviar_jugadores',
					data : {
				   		jugadores_array: jugadores_array
					},
					type : 'POST',
					dataType: 'JSON',
					success  : function(data){
					   	$('#div_nombreJugadores *').hide();
						$('#div_numJugadores *').hide();
						$('#div_preguntas *').show();
						app.ver_pregunta();
					}
				});
			},
			ver_pregunta(){
				app.resetRadioButtons();
				$.ajax({
					url  : url+'datos_turno',
					data : {
					},
					type : 'POST',
					dataType: 'JSON',
					success  : function(data){
					   	$('#div_nombreJugadores *').hide();
						$('#div_numJugadores *').hide();
						$('#div_preguntas *').show();
						if(data.estatus == 'ok'){
							$('#div_preguntas').attr('pregunta_id', data.pregunta_id);
							$('#div_preguntas').attr('jugador_id', data.id);
							$('#div_preguntas').attr('aciertos', data.aciertos);
							$('#pregunta').html(data.pregunta);
							$('#div_preguntas').attr('jugador', data.jugador);
							$('#respuesta1').html(data.respuesta1);
							$('#respuesta2').html(data.respuesta2);
							$('#respuesta3').html(data.respuesta3);
							$('#respuesta4').html(data.respuesta4);
							$('#jugador_turno').html(data.jugador_turno);
							$('#aciertos').html(data.aciertos_frase);
						} else{
							window.open('<?php echo base_url() ?>juego/game_over','_self');
						}
					}
				});
			},
			verificar_respuesta(){
				$.ajax({
					url  : url+'respuesta',
					data : {
				   		pregunta_id: $('#div_preguntas').attr('pregunta_id'),
				   		respuesta: $('input:radio[name=verificar]:checked').val(),
				   		id: $('#div_preguntas').attr('jugador_id'),
				   		aciertos: $('#div_preguntas').attr('aciertos'),
				   		jugador: $('#div_preguntas').attr('jugador')
					},
					type : 'POST',
					dataType: 'JSON',
					success  : function(data){
					   	if(data.respuesta == 'correcta'){
					   		alert('Respuesta correcta!');
					   		app.ver_pregunta();
					   	} else{
					   		alert(data.castigo);
					   		app.ver_pregunta();
					   	}
					}
				});
			},
			resetRadioButtons() {
    			var arRadioBtn = document.getElementsByName('verificar');
    			for (var ii = 0; ii < arRadioBtn.length; ii++) {
     				var radButton = arRadioBtn[ii];
       				radButton.checked = false;
    			}
			},
			iniciar_game_over(){
				app.datos_game_over();
			},
			datos_game_over(){
				$.ajax({
					url  : url+'datos_game_over',
					data : {
					},
					type : 'POST',
					dataType: 'JSON',
					success  : function(data){
					   	$('#div_game_over').empty();
						$.each(data, function(index, item) {
							$('#div_game_over').append('<h4 style="color: white">'+item.no+'.- '+item.nombre+' '+item.aciertos+' ACIERTO(S)</h4>');
						});
					}
				});
			}
		};

		$('#num_jugadores').click(function(){
			num_jugadores = $('#numeroJugadores').val();
			$('#div_nombreJugadores *').show();
			$('#div_numJugadores *').hide();
			$('#div_preguntas *').hide();
			var i = 0;
			$('#espacio_jugadores').empty();
			for(i = 1; i <= num_jugadores; i++){
				$('#espacio_jugadores').append(
					'<div class="row input_nombre"><div class="col-md-12"><input id="jugador'+i+'" class="form-control" type="text" name="Nombres[]" multiple="yes" placeholder="Nombre del jugador '+i+' " required></div></div>'
				);
			}
		})

		$('#enviar_nombres').click(function(){
			var i = 0;
			for(i = 1; i <= num_jugadores; i++){
				jugadores_array.push($('#jugador'+i).val());
			}
			app.enviar_jugadores();
		})

		$('#enviar_respuesta').click(function(){
			app.verificar_respuesta();
		})

		$('#volver_welcome').click(function(){
			window.open('<?php echo base_url() ?>welcome','_self');
		})

		app.<?=$modulo?>();

	});
	</script>
