

<link href="<?php echo base_url() ?>assets/css/styleIndex.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet">
<a href="https://www.flaticon.com/authors/freepik" title="Freepik"></a>
<a href="https://www.flaticon.com/" title="Flaticon"></a>
<body style="background-image: url(<?php echo base_url() ?>assets/img/independencia3.jpg)">
    <div class="container">
        <br>
        <br>
        <br>

        <div class="row">
           <div class="col-md-12 contact-form-jugadores">
            <img src="<?php echo base_url() ?>assets/img/instructivo.png" class="avatar">
            <h2>INSTRUCTIVO</h2>
            <form class="formulario" required>
                <p style="text-align: justify; color:white; font-size: 20px">
                  Histonary es un juego de trivia en línea que te permite competir y divertirte junto con tus amigos. Consiste en una serie de preguntas acerca de la historia de nuestro querido México, donde si te equivocas ¡tendrás un reto o confesión que hacerles a tus compañeros! 
Es ideal para todo tipo de reuniones (con pistache) para los jóvenes de hoy en día que les encanta beber, competir y aprender. Lo mejor es que solo necesitas entender los siguientes pasos: <br><br>
1.  Seleccionar el numero de jugadores que participaran.<br>
2.  Colocar sus nombres en orden de turno.<br>
3.  ¡A contestar preguntas de tu México!<br>
4.  Por cada equivocación se saltará un reto/confesión.<br>
5.  Gana el jugador con mayores aciertos.<br>
                </p>
                <input class="btn btn-warning btn-block"type="button" id="comenzar" value="Comenzar" style="margin-top: 10px; color: white; background-color: red; border-color: red">
            </form>
            </div>
        </div>
    </div>
</body>