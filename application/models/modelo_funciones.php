<?php
class Modelo_funciones extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function ObtenerRegistro($tabla, $where) {
		$this->db->query("SET SQL_BIG_SELECTS=1");
		$query = $this->db->get_where($tabla, $where);
		return $query->row();
	}
	public function ObtenerRegistros($tabla, $where) {
		$this->db->query("SET SQL_BIG_SELECTS=1");
		$this->db->from($tabla);
		if ($where != null) {
			$this->db->where($where);
		}
		$query = $this->db->get();
		return $query->result();
	}
	public function ObtenerRegistrosOrdenados($tabla, $where, $orden) {
		$this->db->query("SET SQL_BIG_SELECTS=1");
		$this->db->from($tabla);
		if ($where != null) {
			$this->db->where($where);
		}
		$this->db->order_by($orden, 'DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function InsertarRegistro($tabla, $data) {
		$this->db->query("SET SQL_BIG_SELECTS=1");
		$query = $this->db->insert($tabla, $data);
		return $query;
	}
	public function InsertarRegistroID($tabla, $data) {
		$this->db->query("SET SQL_BIG_SELECTS=1");
		$this->db->insert($tabla, $data);
		$query = $this->db->insert_id();
		return $query;
	}
	public function EliminarRegistro($tabla, $where) {
		$this->db->query("SET SQL_BIG_SELECTS=1");
		$query = $this->db->delete($tabla, $where);
		return $query;
	}
	public function ActualizarRegistro($tabla, $data, $where) {
		$query = $this->db->update($tabla, $data, $where);
		return $query;
	}
	
	public function FormatoFecha($fecha) {
		$var = explode('-', $fecha);
		return $var[2] . '/' . $var[1] . '/' . $var[0];
	}
	public function VaciarTablas() {
		$query = $this->db->truncate('jugadores');
		return $query;
	}
}
?>