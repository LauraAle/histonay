<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Juego extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function start()
	{
		$data['modulo'] = "iniciar_juego";
		$this->modelo_funciones->VaciarTablas();
		$this->load->view('constructor/header');
		$this->load->view('juego/interface');
		$this->load->view('juego/funciones', $data);
	}

	public function enviar_jugadores(){	
		$data = array();	
		$jugadores = $this->input->post('jugadores_array');
		for ($i=1; $i <= 5; $i++) { //turnos
			for ($k = 0; $k < count($jugadores); $k++) {
				$nombre = $jugadores[$k];
				$data = array('nombre'=>$nombre,'turno'=>$i,'aciertos'=>0,'estatus'=>1);
				$this->modelo_funciones->InsertarRegistro('jugadores',$data);
			}
		}		
		echo 1;
	}
	public function datos_turno() {
		$no_pregunta = rand(1, 12);
		$GetPregunta = $this->modelo_funciones->ObtenerRegistro('preguntas', array('row_id' => $no_pregunta));
		$GetJugador = $this->modelo_funciones->ObtenerRegistro('jugadores', array('estatus' => 1));
		$data = array();
		if($GetJugador){
			//sigue el juego
			$data['estatus'] = 'ok';
			$data['pregunta_id'] = $GetPregunta->row_id;
			$data['pregunta'] = $GetPregunta->descripcion_pregunta;
			$data['respuesta1'] = $GetPregunta->respuesta1;
			$data['respuesta2'] = $GetPregunta->respuesta2;
			$data['respuesta3'] = $GetPregunta->respuesta3;
			$data['respuesta4'] = $GetPregunta->respuesta4;
			$data['id'] = $GetJugador->row_id;
			$data['jugador'] = $GetJugador->nombre;
			$data['aciertos'] = $GetJugador->aciertos;
			$data['aciertos_frase'] = 'Aciertos: ' . $GetJugador->aciertos;
			$data['jugador_turno'] = 'TURNO ' . $GetJugador->turno . ': ' . $GetJugador->nombre;
		}else{
			//juego terminado
			$data['estatus'] = 'error';
		}		
		echo json_encode($data);		
	}
	public function respuesta() {
		$pregunta_id = $this->input->post('pregunta_id');
		$respuesta = $this->input->post('respuesta');
		$id = $this->input->post('id');
		$aciertos = intval($this->input->post('aciertos'));
		$jugador = $this->input->post('jugador');

		$GetPregunta = $this->modelo_funciones->ObtenerRegistro('preguntas', array('row_id' => $pregunta_id));
		$actualizar = $this->modelo_funciones->ActualizarRegistro('jugadores', array('estatus' => 2), array('row_id' => $id));
		$data = array();
		//respuesta correcta
		if(($respuesta == 1) && ($GetPregunta->verificar_respuesta1 == 1) || (($respuesta == 2) && ($GetPregunta->verificar_respuesta2 == 1)) || (($respuesta == 3) && ($GetPregunta->verificar_respuesta3 == 1)) || (($respuesta == 4) && ($GetPregunta->verificar_respuesta4 == 1))){
			$actualizar2 = $this->modelo_funciones->ActualizarRegistro('jugadores', array('aciertos' => ($aciertos+1)), array('nombre' => $jugador));
			$data['respuesta'] = 'correcta';
		//respuesta incorrecta
		}else{
			$no_castigo = rand(1, 10);
			$GetCastigo = $this->modelo_funciones->ObtenerRegistro('castigos', array('row_id' => $no_castigo));
			$data['respuesta'] = 'incorrecta';
			$data['castigo'] = $GetCastigo->descripcion;
		}
		echo json_encode($data);		
	}

	public function game_over() {
		$data['modulo'] = "iniciar_game_over";
		$this->load->view('constructor/header');
		$this->load->view('juego/game_over');
		$this->load->view('juego/funciones', $data);	
	}

	public function datos_game_over() {
		$GetJugadores = $this->modelo_funciones->ObtenerRegistrosOrdenados('jugadores', array('turno' => 5),'aciertos');
		$datos = array();
		$i = 1;
		foreach ($GetJugadores as $key) {
			$datos[$i]['no'] = $i;
			$datos[$i]['nombre'] = strtoupper($key->nombre);
			$datos[$i]['aciertos'] = $key->aciertos;
			$i++;
		}	
		echo json_encode($datos);		
	}
}
