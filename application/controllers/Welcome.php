<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function index()
	{
		$data['modulo'] = "iniciar_welcome";
		$this->load->view('constructor/header');
		$this->load->view('welcome/interface');
		$this->load->view('welcome/funciones', $data);
	}
}
